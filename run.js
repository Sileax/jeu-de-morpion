$('document').ready(function (e) {

    console.log(partie);

    $(".cellule").click(function (e) {

        // Indexes
        let ligne = $(this).closest(".ligne")
        let ligne_index = $(".ligne").index(ligne)

        let cellules_in_line = ".ligne.ligne_" + ligne_index + " .cellule";
        let cellule_index = $(cellules_in_line).index($(this))

        // tester qu'il n'y a pas de pion sur cette case
        if (partie.grille[ligne_index][cellule_index] != null) {

            alert("Il y a deja un pion sur cette case !")

        } else {

            // mettre à jour la grille
            partie.grille[ligne_index][cellule_index] = partie.joueur_courant


            // Poser le pion
            $(this).children(".forme.circle").css('display', 'block');
            
            // changer de joueur
            $(".infos .tour .forme").toggle()

            // tester si la combinaison est gagnante
            partie.joueur_gagnant = partie.combinaison_gagnante();

            // Fin de partie
            if (partie.joueur_gagnant != null) {

                partie.finish();
                $(".cellule").unbind("click");

            } else { // Au tour de l'ordinateur de jouer                

                partie.joueur_courant = 'cross';

                // Recherche aléatoire

                let found = false

                for (let i = 0; i < 3; i++) {
                    for (let j = 0; j < 3; j++) {
                        if (null == partie.grille[i][j]) {
                            partie.grille[i][j] = partie.joueur_courant;
                             // Affichage du pion
                            let cellule_id = i+"_"+j
                            let cellule = document.getElementById(cellule_id)
                            let cross = cellule.querySelector(".forme.cross")
                            cross.style.display = 'block'
                            found = true
                            break;
                        }
                    }
                    
                    if (found == true) {
                        break
                    }
                }
                
                // Tester si une combinaison est gagnante
                partie.joueur_gagnant = partie.combinaison_gagnante();

                // Fin de partie
                if (partie.joueur_gagnant != null) {

                    partie.finish();
                    $(".cellule").unbind("click");

                }else{ // Au tour du joueur

                    partie.joueur_courant = 'circle'
                    
                }

            }

        }

    })

});