let partie = {

    joueur_courant:"circle",
    joueur_gagnant:null,

    grille:[
        [null,null,null],
        [null,null,null],
        [null,null,null],
    ],

    combinaisons_gagnantes:[
        [[0,0],[0,1],[0,2]], // ligne 1
        [[1,0],[1,1],[1,2]],
        [[2,0],[2,1],[2,2]],
        [[0,0],[1,0],[2,0]], // colonne 1
        [[0,1],[1,1],[2,1]],
        [[0,2],[1,2],[2,2]],
        [[0,0],[1,1],[2,2]], // diagonales
        [[0,2],[1,1],[2,0]],
    ],

    finish: function(){
        alert('fin de partie')
    },

    combinaison_gagnante: function(){

        partie.joueur_gagnant = null

        for(let i=0;i<3;i++){
            if(partie.joueur_courant == partie.grille[i][0] &&  partie.joueur_courant == partie.grille[i][1] &&  partie.joueur_courant == partie.grille[i][2]){
                partie.joueur_gagnant = partie.joueur_courant;
                break;
            }

            if(partie.joueur_courant == partie.grille[0][i] &&  partie.joueur_courant == partie.grille[1][i] &&  partie.joueur_courant == partie.grille[2][i]){
                partie.joueur_gagnant = partie.joueur_courant; 
                break;
            }
        }

        if(partie.joueur_courant == partie.grille[0][0] &&  partie.joueur_courant == partie.grille[1][1] &&  partie.joueur_courant == partie.grille[2][2]){
            partie.joueur_gagnant = partie.joueur_courant;
        }


        if(  partie.joueur_courant == partie.grille[2][0] &&  partie.joueur_courant == partie.grille[1][1] &&  partie.joueur_courant == partie.grille[0][2]){
            partie.joueur_gagnant = partie.joueur_courant;
        }

        return partie.joueur_gagnant
    }
}